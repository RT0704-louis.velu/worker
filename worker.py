import functions
import os
import time
import requests
import json
#config:
SERVER_IP="127.0.0.1"
SERVER_PORT="5000"
GIT_LOCATION = "gitlab.com/RT0704-louis.velu/"
WORKER_ID = 1
#routine d'execution
while True:
	try:
		#on va chercher un tache sur le server; sinon on recommence le temps qu'il n'y en a pas de dispo
		data = {"worker_id":WORKER_ID}
		#print(json.dumps(data))
		resp = requests.get("http://"+SERVER_IP+":"+SERVER_PORT+"/get-job", params={"data":json.dumps(data)})
	except:
		print("server introuvable")
		continue
	#si la réponse n'est pas bonne (200)
	if resp.status_code != 200:
		print("Server IP: "+SERVER_IP+" Server Port: "+SERVER_PORT+" HTTP satus code: "+ str(resp.status_code))
		print("reponse: "+ str(resp))
		time.sleep(1)
		continue
	#si le server n'a pas de tache disponible
	tache = json.loads(resp.content.decode('utf-8'))
	if "erreur" in tache:
		time.sleep(1)
		continue


	print("Start job"+tache["id"])
	#on verifie que la tache est bien connue
	if not tache["job"]["type"] in ["graphviz", "pandoc", "image_magick"]:
		#sinon on remonte l'erreur au server
		data = {"worker_id":WORKER_ID, "job_id":tache["id"], "erreur":"Type de job inconnu"}
		requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/put-result", data={"data":json.dumps(data)})
		continue

	#on verifie avoir le docker
	try:
		if not  os.path.exists("./docker/"+tache["job"]["type"]+"/Dockerfile"):
			print("Downlaod docker " + tache["job"]["type"])
			#si on n'a pas le Dockerfile, on va le chercher dans la base de dockerfile
			functions.git_clone(GIT_LOCATION+"docker", "./docker/git")
			os.system("mv ./docker/git/"+tache["job"]["type"]+" ./docker/")
			functions.git_rm("./docker/git")
		#puis on le build
		print("build docker " + tache["job"]["type"])
		functions.docker_build("./docker/"+tache["job"]["type"], tache["job"]["type"]+":latest")
	except:
		print("erreur docker")
		#si une erreur survient on remonte l'erreur au server
		data = {"worker_id":WORKER_ID, "job_id":tache["id"], "erreur":"impossible de trouver le bon dockerfile"}
		requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/put-result", data={"data":json.dumps(data)})
		continue

	#télécharger le fichier de la tache
	try:
		functions.git_clone(GIT_LOCATION+"file_in.git", "./tmp/file_in/git")
		os.system("mv ./tmp/file_in/git/"+tache["id"]+" ./tmp/file_in/")
		functions.git_rm("./tmp/file_in/git")
	except:
		print("erreur download")
                #si une erreur survient on remonte l'erreur au server
		data = {"worker_id":WORKER_ID, "job_id":tache["id"], "erreur":"impossible de trouver le document a traiter"}
		requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/put-result", data={"data":json.dumps(data)})
		continue

	#on execute le docker
	try:
		## TODO cout du traitement
		print(os.path.abspath("./tmp/"))
		os.system("mkdir -p ./tmp/resultat/"+tache["id"])
		functions.docker_run(tache["job"]["type"], environment=["DIR="+tache["id"],"FILENAME_IN="+tache["job"]["file_in"],"FILENAME_OUT="+tache["job"]["file_out"] ,"FORMAT="+tache["job"]["parametre"]["format"], "ARGS="+tache["job"]["parametre"]["args"]], volumes={os.path.abspath("./tmp/"): {"bind": "/tmp", "mode": "rw"}})
	except:
		print("erreur exec")
                #si une erreur survient on remonte l'erreur au server
		data = {"worker_id":WORKER_ID, "job_id":tache["id"], "erreur":"erreur lors du traitement"}
		requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/put-result", data={"data":json.dumps(data)})
		continue


	#on upload le fichier
	try:
		functions.git_clone(GIT_LOCATION+"file_out.git","./tmp/file_out/git")
		functions.git_add("./tmp/file_out/git", "./tmp/resultat/"+tache["id"])
		functions.git_push("./tmp/file_out/git")
		functions.git_rm("./tmp/file_out/git")
	except:
		print("erreur upload")
                #si une erreur survient on remonte l'erreur au server
		data = {"worker_id":WORKER_ID, "job_id":tache["id"], "erreur": "impossible d'uploader le fichier traité"}
		requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/put-result", data={"data":json.dumps(data)})
		os.system("rm -rf ./tmp")
		continue

	#on notifie la fin de tache
       	### TODO stats du traitement
	data = {"worker_id":WORKER_ID, "job_id":tache["id"], "filename":tache["job"]["file_out"], "stats": 0}
	requests.post("http://"+SERVER_IP+":"+SERVER_PORT+"/put-result", data={"data":json.dumps(data)})
