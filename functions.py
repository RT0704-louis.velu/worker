import pika
import docker
import os

def create_queue(host, queue_name):
	connection = pika.BlockingConnection(pika.ConnectionParameters(host=host))
	channel = connection.channel()
	channel.queue_declare(queue=queue_name)
	connection.close()
	print("Create : queue named " + queue_name)

def publish(host, queue_name, body):
	connection = pika.BlockingConnection(pika.ConnectionParameters(host=host))
	channel = connection.channel()
	channel.basic_publish(exchange='', routing_key=queue_name, body=body)
	connection.close()
	print("Send : to queue "+ queue_name +" message : " + body)


def consume(host, queue_name):
	connection = pika.BlockingConnection(pika.ConnectionParameters(host=host))
	channel = connection.channel()
	method_frame, header_frame, body = channel.basic_get(queue_name)
	if method_frame:
		channel.basic_ack(method_frame.delivery_tag)
		print("Read : queue " + queue_name)
		print(method_frame, header_frame, body)
	else:
		print("Read : queue " + queue_name + "Nothing to read")
	connection.close()
	return method_frame, header_frame, body

def docker_run(docker_name, detach=False, ports={}, environment=[], volumes={}):
	client = docker.from_env()
	client.containers.run(docker_name, detach=detach, ports=ports, environment=environment, volumes=volumes)


def docker_build(path, tag):
	client = docker.api.client.APIClient()
	client.build(path, tag=tag, rm=True, quiet=True)

def docker_ps():
	client = docker.from_env()
	return client.containers.list()


token="DPkuxQZwXnZzEEVW2KL2"
os.system("git config --global user.email \"louis.velu@gmail.com\"")
os.system("git config --global user.name \"louis.velu\"")

def git_clone(source, target="./tmp"):
	source = source.replace('&', '').replace('|', '')
	source = source.replace("https://", '').replace("http://", '')
	target = target.replace('&', '').replace('|', '')
	os.system("git clone https://oauth:"+ token + "@" + source + " " + target)

def git_init(target="./tmp"):
	target = target.replace('&', '').replace('|', '')
	os.system("git init " + target)

def git_add(repo_path, file_path):
	repo_path = repo_path.replace('&', '').replace('|', '')
	file_path = file_path.replace('&','').replace('|','')
	os.system("cp -R "+ file_path + " " + repo_path)
	os.system("cd "+ repo_path  +" && git add .")

def git_push(repo_path="./tmp"):
	repo_path = repo_path.replace('&','').replace('|','')
	os.system("cd " + repo_path + " && git commit -m update")
	os.system("cd " + repo_path + " && git push")

def git_rm(target="./tmp"):
	target = target.replace('&','').replace('|','')
	os.system("rm -rf --interactive=never " + target)
